<div>
    <b>Date time: {{ date('d M Y h:i:s A') }}</b>
    <hr>
    ipaddress : {{ $visitor['ipaddress'] ?? 'N/A' }} <br>
    iso_code : {{ $visitor['iso_code'] ?? 'N/A' }} <br>
    country : {{ $visitor['country'] ?? 'N/A' }} <br>
    city : {{ $visitor['city'] ?? 'N/A' }} <br>
    state : {{ $visitor['state'] ?? 'N/A' }} <br>
    state_name : {{ $visitor['state_name'] ?? 'N/A' }} <br>
    postal_code : {{ $visitor['postal_code'] ?? 'N/A' }} <br>
    lat : {{ $visitor['lat'] ?? 'N/A' }} <br>
    lon : {{ $visitor['lon'] ?? 'N/A' }} <br>
    timezone : {{ $visitor['timezone'] ?? 'N/A' }} <br>
    continent : {{ $visitor['continent'] ?? 'N/A' }} <br>
    currency : {{ $visitor['currency'] ?? 'N/A' }} <br>
    default : {{ $visitor['default'] ?? 'N/A' }} <br>
    cached : {{ $visitor['cached'] ?? 'N/A' }} <br>
    browser : {{ $visitor['browser'] ?? 'N/A' }} <br>
    device : {{ $visitor['device'] ?? 'N/A' }} <br>
    os : {{ $visitor['os'] ?? 'N/A' }} <br>
    url : {{ $visitor['url'] ?? 'N/A' }} <br>
</div>