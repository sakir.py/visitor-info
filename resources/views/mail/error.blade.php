<div>
    <b>Date time: {{ date('d M Y h:i:s A') }}</b>
    <hr>
    <b>Error message: </b> <p>{{ $error_message }}</p>
</div>