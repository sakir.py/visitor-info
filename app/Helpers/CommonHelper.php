<?php

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;

if (!function_exists('random_code')){

    function upload_image($folder_path, $image, $image_new_name = null){
        if ($image && file_exists($image->getRealPath())) {
            if (!file_exists($folder_path)) {
                mkdir($folder_path, 0777, true);
            }
            if ($image_new_name){
                $image_folder_path_with_name = $folder_path . $image_new_name . '.' . $image->getClientOriginalExtension();
            }else{
                $image_folder_path_with_name = $folder_path . $image->getClientOriginalName();
            }
            Image::make($image->getRealPath())->save($image_folder_path_with_name);
            return $image_folder_path_with_name;
        }
        return false;
    }

    function delete_image($image){
        try {
            if ($image)
                File::delete(public_path($image));
        }catch (\Exception$exception){

        }
    }


}
