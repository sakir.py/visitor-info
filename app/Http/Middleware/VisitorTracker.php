<?php

namespace App\Http\Middleware;

use App\Mail\VisitorInfoError;
use App\Mail\VisitorInfoMail;
use App\Models\BlockIPAddress;
use App\Models\ExceptionMessage;
use App\Models\VisitorInformation;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class VisitorTracker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $ip = geoip()->getClientIP();
            $visitor['ipaddress'] =   $ip;
            $visitor['iso_code']  =   geoip()->getLocation($ip)->iso_code;
            $visitor['country']   =   geoip()->getLocation($ip)->country;
            $visitor['city']      =   geoip()->getLocation($ip)->city;
            $visitor['state']     =   geoip()->getLocation($ip)->state;
            $visitor['state_name']    =   geoip()->getLocation($ip)->state_name;
            $visitor['postal_code']   =   geoip()->getLocation($ip)->postal_code;
            $visitor['lat']       =   geoip()->getLocation($ip)->lat;
            $visitor['lon']       =   geoip()->getLocation($ip)->lon;
            $visitor['timezone']  =   geoip()->getLocation($ip)->timezone;
            $visitor['continent'] =   geoip()->getLocation($ip)->continent;
            $visitor['currency']  =   geoip()->getLocation($ip)->currency;
            $visitor['default']   =   geoip()->getLocation($ip)->default;
            $visitor['cached']    =   geoip()->getLocation($ip)->cached;
            $visitor['browser']   =   get_client_browser();
            $visitor['device']    =   get_client_device();
            $visitor['os']        =   get_client_os();
            $visitor['url']       =   $request->url();
            Mail::to('m.sakirahmed@gmail.com')->send(new VisitorInfoMail($visitor));
        } catch (\Exception $exception){
            Mail::to('m.sakirahmed@gmail.com')->send(new VisitorInfoError($exception->getMessage()));
        }
        return $next($request);
    }
}
